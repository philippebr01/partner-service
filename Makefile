build:
	docker-compose build

up:
	docker-compose up -d

logs:
	docker-compose logs --tail=25 api fluentbit

down:
	docker-compose down --remove-orphans

test:
	PYTHONPATH=tests/ pipenv run pytest


all: down build up
