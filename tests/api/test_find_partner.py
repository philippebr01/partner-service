import unittest
from app.main import app
from fastapi.testclient import TestClient
from app.domain.partner import Partner
from geojson import Point, MultiPolygon
from mock import patch
from app.domain.exception.custom_exception import CustomException

SAVED_PARTNER = Partner(2, 'Example owner', 'Trading name', '999999999999999',
                        Point((-115.81, 37.24)), 
                        MultiPolygon([([(3.78, 9.28), (-130.91, 1.52), (35.12, 72.234), (3.78, 9.28)],),
                                      ([(23.18, -34.29), (-1.31, -4.61), (3.41, 77.91), (23.18, -34.29)],)]))

NOT_FOUND_EXCEPTION=CustomException('NOT_FOUND', 'Partner not found with this id')

class TestFindPartner(unittest.TestCase):

    def setUp(self):
        self.app = TestClient(app)

    def tearDown(self):
        pass

    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.find', return_value=SAVED_PARTNER)    
    def test_success(self, mock_repository):
        res = self.app.get('/partners/2')

        status_code = res.status_code
        json = res.json()

        self.assertEqual(200, status_code)
        self.assertEqual(2, json['id'])

    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.find', side_effect=NOT_FOUND_EXCEPTION)    
    def test_not_found(self, mock_repository):
        res = self.app.get('/partners/2')

        status_code = res.status_code
        json = res.json()

        self.assertEqual(404, status_code)
        self.assertEqual('NOT_FOUND', json['code'])