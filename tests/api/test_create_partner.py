import unittest
from app.main import app
from fastapi.testclient import TestClient
from app.domain.partner import Partner
from geojson import Point, MultiPolygon
from mock import patch
from app.domain.exception.custom_exception import CustomException

SAVED_PARTNER = Partner(2, 'Example owner', 'Trading name', '999999999999999',
                        Point((-115.81, 37.24)), 
                        MultiPolygon([([(3.78, 9.28), (-130.91, 1.52), (35.12, 72.234), (3.78, 9.28)],),
                                      ([(23.18, -34.29), (-1.31, -4.61), (3.41, 77.91), (23.18, -34.29)],)]))

PARTNER = {
  "tradingName": "Adega Osasco",
  "ownerName": "Ze da Ambev",
  "document": "02.453.716/000170",
  "coverageArea": {
    "type": "MultiPolygon",
    "coordinates": [
      [
        [
          [
            -43.36556,
            -22.99669
          ],
          [
            -43.36539,
            -23.01928
          ],
          [
            -43.26583,
            -23.01802
          ],
          [
            -43.25724,
            -23.00649
          ],
          [
            -43.23355,
            -23.00127
          ],
          [
            -43.2381,
            -22.99716
          ],
          [
            -43.23866,
            -22.99649
          ],
          [
            -43.24063,
            -22.99756
          ],
          [
            -43.24634,
            -22.99736
          ],
          [
            -43.24677,
            -22.99606
          ],
          [
            -43.24067,
            -22.99381
          ],
          [
            -43.24886,
            -22.99121
          ],
          [
            -43.25617,
            -22.99456
          ],
          [
            -43.25625,
            -22.99203
          ],
          [
            -43.25346,
            -22.99065
          ],
          [
            -43.29599,
            -22.98283
          ],
          [
            -43.3262,
            -22.96481
          ],
          [
            -43.33427,
            -22.96402
          ],
          [
            -43.33616,
            -22.96829
          ],
          [
            -43.342,
            -22.98157
          ],
          [
            -43.34817,
            -22.97967
          ],
          [
            -43.35142,
            -22.98062
          ],
          [
            -43.3573,
            -22.98084
          ],
          [
            -43.36522,
            -22.98032
          ],
          [
            -43.36696,
            -22.98422
          ],
          [
            -43.36717,
            -22.98855
          ],
          [
            -43.36636,
            -22.99351
          ],
          [
            -43.36556,
            -22.99669
          ]
        ]
      ]
    ]
  },
  "address": {
    "type": "Point",
    "coordinates": [
      -43.297337,
      -23.013538
    ]
  }
}


PARTNER_BAD_REQUEST = {
  "tradingName": "Adega Osasco",
  "ownerName": "Ze da Ambev",
  "document": "02.453.716/000170",
  "address": {
    "type": "Point",
    "coordinates": [
      -43.297337,
      -23.013538
    ]
  }
}

details = [{
    'issue':'There is already a partner registered with this document.',
    'field':'document',
    'location': 'body'
}]
ALREADY_EXISTS_EXCEPTION=CustomException('BAD_REQUEST', 'Already registered', details)

class TestCreatePartner(unittest.TestCase):
    
    def setUp(self):
        self.app = TestClient(app)

    def tearDown(self):
        pass

    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.save', return_value=SAVED_PARTNER)    
    def test_success(self, mock_repository):
        res = self.app.post('/partners',
                            headers={'content-type': 'application/json'},
                            json=PARTNER)
        self.assertEqual('/partners/2', res.headers['Location'])

    def test_fail_required_property(self):        
        res = self.app.post('/partners',
                            headers={'content-type': 'application/json'},
                            json=PARTNER_BAD_REQUEST)
        json = res.json()
        status_code = res.status_code

        self.assertEqual(400, status_code)
        self.assertEqual('BAD_REQUEST', json['code'])
        self.assertEqual('coverageArea', json['details'][0]['field'])

    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.save', side_effect=ALREADY_EXISTS_EXCEPTION)    
    def test_fail_already_exists(self, mock_repository):
        res = self.app.post('/partners',
                            headers={'content-type': 'application/json'},
                            json=PARTNER)

        json = res.json()
        status_code = res.status_code

        self.assertEqual(400, status_code)
        self.assertEqual('BAD_REQUEST', json['code'])
        self.assertEqual('document', json['details'][0]['field'])