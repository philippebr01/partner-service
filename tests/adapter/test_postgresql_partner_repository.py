import os
os.environ['POSTGRESQL_CONN_STRING'] = 'postgresql+psycopg2://username:password@localhost:5432/partners_db'

import unittest
from typing import List
from mock import patch
from geojson import Point, MultiPolygon
from app.domain.exception.custom_exception import CustomException
from app.domain.partner import Partner
from app.adapter.postgresql_partner_repository import PostgreSQLPartnerRepository



SAVED_PARTNER = Partner(2, 'Example owner', 'Trading name', '999999999999999',
                        Point((-115.81, 37.24)), 
                        MultiPolygon([([(3.78, 9.28), (-130.91, 1.52), (35.12, 72.234), (3.78, 9.28)],),
                                      ([(23.18, -34.29), (-1.31, -4.61), (3.41, 77.91), (23.18, -34.29)],)]))

details = [{
    'issue':'There is already a partner registered with this document.',
    'field':'document',
    'location': 'body'
}]
ALREADY_EXISTS_EXCEPTION=CustomException('BAD_REQUEST', 'Already registered', details)
NOT_FOUND_EXCEPTION=CustomException('NOT_FOUND', 'Partner not found with this id')



class TestPostgresqlPartnerRepository(unittest.TestCase):

    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.save', return_value=SAVED_PARTNER)    
    def test_save(self, mock_repository):
        repository = PostgreSQLPartnerRepository()

        new_partner = Partner(None, 'Example owner', 'Trading name', '999999999999999',
                              Point((-115.81, 37.24)), 
                              MultiPolygon([([(3.78, 9.28), (-130.91, 1.52), (35.12, 72.234), (3.78, 9.28)],),
                                            ([(23.18, -34.29), (-1.31, -4.61), (3.41, 77.91), (23.18, -34.29)],)]))
        saved = repository.save(new_partner)

        self.assertEqual(new_partner.ownerName, saved.ownerName)
        self.assertEqual(2, saved.id)
        

    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.save', side_effect=ALREADY_EXISTS_EXCEPTION)    
    def test_save_already_exist(self, mock_repository):
        repository = PostgreSQLPartnerRepository()

        new_partner = Partner(None, 'Example owner', 'Trading name', '999999999999999',
                              Point((-115.81, 37.24)), 
                              MultiPolygon([([(3.78, 9.28), (-130.91, 1.52), (35.12, 72.234), (3.78, 9.28)],),
                                            ([(23.18, -34.29), (-1.31, -4.61), (3.41, 77.91), (23.18, -34.29)],)]))
        
        code = ''
        try:
            saved = repository.save(new_partner)
        except CustomException as ex:
            code = ex.code

        self.assertEqual('BAD_REQUEST', code)


    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.find', return_value=SAVED_PARTNER)    
    def test_find(self, mock_repository):
        repository = PostgreSQLPartnerRepository()
        saved = repository.find(2)

        self.assertEqual(2, saved.id)

    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.find', side_effect=NOT_FOUND_EXCEPTION)    
    def test_find_not_found(self, mock_repository):
        repository = PostgreSQLPartnerRepository()
        code = ''
        try:
            saved = repository.find(2)
        except CustomException as ex:
            code = ex.code
        self.assertEqual('NOT_FOUND', code)


    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.find_closer', return_value=[SAVED_PARTNER])    
    def test_find_closer(self, mock_repository):
        repository = PostgreSQLPartnerRepository()
        result = repository.find_closer(-43.297337,-23.013538)
        
        self.assertEqual(2, result[0].id)


    @patch('app.adapter.postgresql_partner_repository.PostgreSQLPartnerRepository.find_closer', return_value=[])    
    def test_find_closer_empty(self, mock_repository):
        repository = PostgreSQLPartnerRepository()
        result = repository.find_closer(-43.297337,-23.013538)

        self.assertFalse(result)
        
        