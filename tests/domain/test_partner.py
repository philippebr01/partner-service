import unittest
from app.domain.partner import Partner
from geojson import Point, MultiPolygon

class TestPartner(unittest.TestCase):

    def test_new_partner(self):
        partner = Partner(2, 'Example owner', 'Trading name', '999999999999999',
                          Point((-115.81, 37.24)), 
                          MultiPolygon([([(3.78, 9.28), (-130.91, 1.52), (35.12, 72.234), (3.78, 9.28)],),
                                        ([(23.18, -34.29), (-1.31, -4.61), (3.41, 77.91), (23.18, -34.29)],)]))
        self.assertEqual(2, partner.id)
        self.assertEqual('Example owner', partner.ownerName)
