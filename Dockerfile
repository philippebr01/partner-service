# Update the VARIANT arg in devcontainer.json to pick a Python version: 3, 3.8, 3.7, 3.6 
ARG VARIANT=3.6-slim
ARG APP_PORT=8081
FROM python:${VARIANT}

RUN pip install pipenv

COPY Pipfile.lock /tmp/Pipfile.lock
COPY Pipfile /tmp/Pipfile

WORKDIR /tmp
RUN pipenv install --system


RUN adduser --disabled-password worker
USER worker
WORKDIR /home/worker

ENV PATH="/home/worker/.local/bin:${PATH}"
ENV PYTHONPATH="$PYTHONPATH:app"
ENV APP_PORT 8081

COPY ./ /home/worker/src/

COPY --chown=worker:worker . .

WORKDIR /home/worker

CMD [ "sh", "-c", "uvicorn main:app --host 0.0.0.0 --port $APP_PORT" ]
