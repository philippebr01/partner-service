import os
os.environ['POSTGRESQL_CONN_STRING'] = 'postgresql+psycopg2://username:password@localhost:5432/partners_db'
import uvicorn
from app.main import app

if __name__ == '__main__':
    uvicorn.run(app, port=8082)