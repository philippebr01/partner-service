from os import stat
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from fastapi.requests import Request
from typing import List
from app.domain.partner import Partner
from app.adapter.postgresql_partner_repository import PostgreSQLPartnerRepository
from pydantic import BaseModel
from geojson import Point, MultiPolygon
from app.domain.exception.custom_exception import CustomException
from app.domain.exception.custom_exception_handler import CustomExceptionHandler
from fastapi.exceptions import RequestValidationError
from fastapi.encoders import jsonable_encoder


repository = PostgreSQLPartnerRepository()

class CreatePartner(BaseModel):
    ownerName: str
    tradingName: str
    document: str
    address: Point
    coverageArea: MultiPolygon

class FindPartner(BaseModel):
    id: int
    ownerName: str
    tradingName: str
    document: str
    address: Point
    coverageArea: MultiPolygon

class ErrorDetail(BaseModel):
    issue: str
    field: str
    location: str

    def _asdict(self):
        return {
            'issue': self.issue,
            'location': self.location,
            'field': self.field
        }

class Error(BaseModel):
    code: str
    message: str
    details: List[ErrorDetail]

app = FastAPI()


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, ex: RequestValidationError):    
    details = []
    for error in ex.errors():
        detail = {'issue':error['msg'],
                  'location':error['loc'][0],
                  'field':error['loc'][1]}
        details.append(detail)

    handler = CustomExceptionHandler()
    status_code, content = handler.handle(CustomException('BAD_REQUEST', 'Please check your request info', details=details))
    return JSONResponse(
        status_code=status_code,
        content=content,
    )

@app.exception_handler(CustomException)
async def custom_exception_handler(request: Request, ex: CustomException):
    handler = CustomExceptionHandler()
    status_code, content = handler.handle(ex)
    return JSONResponse(
        status_code=status_code,
        content=content,
    )

@app.exception_handler(Exception)
async def default_exception_handler(request: Request, ex: Exception):
    return JSONResponse(
        status_code=500,
        content={'code': 'INTERNAL_ERROR', 'message': 'Internal server error. Please contact the sys admin.'},
    )


@app.post('/partners', status_code=201, responses={400: {'model': Error}, 401: {'model': Error}, 403: {'model': Error}, 422: {'model': Error}, 500: {'model': Error} })
def create(payload: CreatePartner):
    partner = Partner(id=None,
                      ownerName=payload.ownerName,
                      tradingName=payload.tradingName,
                      document=payload.document,
                      address=payload.address,
                      coverageArea=payload.coverageArea)
    partner = partner.save(repository=repository)
    return JSONResponse(headers={'Location': '/partners/%s' % partner.id})

@app.get('/partners', status_code=200, response_model=List[FindPartner], responses={400: {'model': Error}, 401: {'model': Error}, 403: {'model': Error}, 422: {'model': Error}, 500: {'model': Error} })
def find_closer(lat: float, lng: float):
    partners = repository.find_closer(lat, lng)
    body = []
    for partner in partners:
        body.append(FindPartner(id=partner.id, 
                                ownerName=partner.ownerName,
                                tradingName=partner.tradingName,
                                document=partner.document,
                                address=partner.address,
                                coverageArea=partner.coverageArea))
                                
    return JSONResponse(content=jsonable_encoder(body))


@app.get('/partners/{id}', status_code=200, response_model=FindPartner, responses={400: {'model': Error}, 401: {'model': Error}, 403: {'model': Error}, 404: {'model': Error}, 422: {'model': Error}, 500: {'model': Error} })
def find(id: int):
    result = repository.find(id)

    body = FindPartner(id=result.id, 
                       ownerName=result.ownerName,
                       tradingName=result.tradingName,
                       document=result.document,
                       address=result.address,
                       coverageArea=result.coverageArea)
    return JSONResponse(content=jsonable_encoder(body))

