from dataclasses import dataclass
from geojson import Point, MultiPolygon

@dataclass
class Partner():    
    id: int
    ownerName: str
    tradingName: str
    document: str
    address: Point
    coverageArea: MultiPolygon


    def save(self, repository: 'PartnerRepository'):
        return repository.save(self)