import abc
from typing import List
from app.domain.partner import Partner

class PartnerRepository(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def save(self, partner: Partner) -> Partner:
        raise NotImplementedError

    @abc.abstractmethod
    def find(self, id: int) -> Partner:
        raise NotImplementedError

    @abc.abstractmethod
    def find_closer(self, lat: float, lng: float) -> List[Partner]:
        raise NotImplementedError