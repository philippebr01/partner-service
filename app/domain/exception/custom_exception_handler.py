from app.domain.exception.custom_exception import CustomException

class CustomExceptionHandler:

    def __init__(self) -> None:
        self.errors = {
            'NOT_FOUND': 404,
            'BAD_REQUEST': 400
        }

    def handle(self, exception: CustomException):
        try:
            status_code = self.errors[exception.code]
        except KeyError as err:
            return 500, {'code': 'INTERNAL_ERROR', 'message': 'Internal server error. Please contact the sys admin.'}
        return status_code, exception._asdict()