
class CustomException(Exception):

    def __init__(self, code, message, details=None) -> None:
        super().__init__(message)
        self.message = message
        self.code = code
        self.details = details

    def _asdict(self):
        return {
            'code': self.code,
            'message': self.message,
            'details': self.details
        }