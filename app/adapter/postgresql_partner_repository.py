import os
import json
import sqlalchemy as db
from app.domain.partner_repository import PartnerRepository
from app.domain.partner import Partner
from typing import List
from app.domain.exception.custom_exception import CustomException
from sqlalchemy.exc import IntegrityError

engine = db.create_engine(os.getenv('POSTGRESQL_CONN_STRING'))


class PostgreSQLPartnerRepository(PartnerRepository):

    def __init__(self):
        self.conn = engine.connect()

    def save(self, partner: Partner) -> Partner:
        query = """
        insert into partners (partner_owner_name, partner_trading_name, partner_document,
                            partner_address, partner_coverage_area)
        values ('%s', '%s', '%s', 
                ST_SetSRID(ST_GeomFromGeoJSON('%s'), 4326),  
                ST_SetSRID(ST_GeomFromGeoJSON('%s'), 4326))
        RETURNING partner_id;
        """
        try:
            rows = self.conn.execute(query % (partner.ownerName,
                                              partner.tradingName,
                                              partner.document,
                                              json.dumps(partner.address),
                                              json.dumps(partner.coverageArea)))
        except IntegrityError as err:
            details = [{
                'field': 'document',
                'issue': 'There is already a partner registered with this document',
                'location': 'body'
            }]
            raise CustomException("Conflict with partners", "BAD_REQUEST", details)
        for row in rows:
            partner.id = row[0]
        
        return partner
    
    def find(self, id: int) -> Partner:
        query = """
            select p.partner_id,
                   p.partner_owner_name,
                   p.partner_trading_name,
                   p.partner_document,
                   ST_AsGeoJSON(p.partner_address),
                   ST_AsGeoJSON(p.partner_coverage_area)
            from partners p
            where p.partner_id = %d
        """
                
        rows = self.conn.execute(query % id)
        partners = []
        for row in rows:
            return Partner(id=row[0],
                           ownerName=row[1],
                           tradingName=row[2],
                           document=row[3],
                           address=json.loads(row[4]),
                           coverageArea=json.loads(row[5]))
        return None

    def find_closer(self, lat: float, lng: float) -> List[Partner]:
        query = "select " \
                 "p.partner_id, " \
                 "p.partner_owner_name, " \
                 "p.partner_trading_name, " \
                 "p.partner_document, " \
                 "ST_AsGeoJSON(p.partner_address), " \
                 "ST_AsGeoJSON(p.partner_coverage_area), " \
                 "ST_Distance(p.partner_address,ST_SetSRID(ST_Point("+str(lat)+","+str(lng)+"),4326)) as partner_distance " \
                 "from partners p " \
                 "where ST_Contains(p.partner_coverage_area, ST_SetSRID(ST_Point("+str(lat)+","+str(lng)+"),4326))" \
                 "order by p.partner_address  <-> ST_SetSRID(ST_Point("+str(lat)+","+str(lng)+"),4326);"
                
        rows = self.conn.execute(query)
        partners = []
        for row in rows:
            partners.append(Partner(id=row[0],
                                    ownerName=row[1],
                                    tradingName=row[2],
                                    document=row[3],
                                    address=json.loads(row[4]),
                                    coverageArea=json.loads(row[5])))
        return partners
        
        